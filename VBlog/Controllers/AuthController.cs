﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using VBlog.ViewModels;

namespace VBlog.Controllers
{
    public class AuthController : Controller
    {
        //
        // GET: /Auth/
        public ActionResult Login()
        {

            return View(new AuthLogin()
            {
                
            });
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToRoute("home");
        }

        [HttpPost]
        public ActionResult Login(AuthLogin formAuthLogin, string returnUrl)
        {
            if(!ModelState.IsValid)
            return View(formAuthLogin);

            //if (formAuthLogin.UserName != "Vadmin")
            //{
            //    ModelState.AddModelError("UserName","User name or password is invalid!");
            //    return View(formAuthLogin);
            //}

            FormsAuthentication.SetAuthCookie(formAuthLogin.UserName, true);

            if (!string.IsNullOrWhiteSpace(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToRoute("home");
        }
	}
}